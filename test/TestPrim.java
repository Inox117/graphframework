import graphe.ValuatedUndirectedMatrix;
import org.junit.Test;
import outils.Algo;
import outils.GraphTools;

import static org.junit.Assert.assertTrue;

public class TestPrim {

    int[][] matriceTest = {
            {0,1,2,4},
            {1,0,1,6},
            {2,1,0,3},
            {4,6,3,0}
    };

    int[][] matriceAttendus = {
            {0,1,Integer.MAX_VALUE,Integer.MAX_VALUE},
            {1,0,1,Integer.MAX_VALUE},
            {Integer.MAX_VALUE,1,0,3},
            {Integer.MAX_VALUE,Integer.MAX_VALUE,3,0}
    };

    @Test
    public void testPrim(){
        ValuatedUndirectedMatrix g = new ValuatedUndirectedMatrix(matriceTest);
        ValuatedUndirectedMatrix res = Algo.prim(g);
        assertTrue(GraphTools.MatrixEqualsAnotherMatrix(matriceAttendus,res.toAdjacencyMatrix()));
    }


}
