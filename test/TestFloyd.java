import graphe.ValuatedDirectedMatrix;
import graphe.ValuatedUndirectedMatrix;
import org.junit.Test;
import outils.Algo;
import outils.GraphTools;

import static org.junit.Assert.assertTrue;

public class TestFloyd {

    int[][] matriceTest = {
            {0,3,Integer.MAX_VALUE,3},
            {2,0,2,2},
            {-2,Integer.MAX_VALUE,0,1},
            {Integer.MAX_VALUE,4,4,0}
    };

    int[][] plusCourtCheminAttendus = {
            {0,3,5,3},
            {0,0,2,2},
            {-2,1,0,1},
            {2,4,4,0}
    };

    @Test
    public void testFloyd(){
        ValuatedDirectedMatrix g = new ValuatedDirectedMatrix(matriceTest);
        GraphTools.showMatrice(Algo.floyd(g));
        assertTrue(GraphTools.MatrixEqualsAnotherMatrix(plusCourtCheminAttendus, Algo.floyd(g)));
    }

}
