package outils;

import graphe.Arc;
import graphe.IDirectedGraph;
import graphe.IUndirectedGraph;

import java.util.*;

public class GraphTools {

    public static int[][] generateGraphData(int n, int m, boolean s){
        if(m>(((n*n)-n)/2) || m < 0){
            return null;
        }
        int[][] matriceAdjacence = new int[n][n];
        ArrayList<Arc> arcs = new ArrayList<Arc>();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                if(j != i){
                    arcs.add(new Arc(i, j));
                }
            }
        }
        Collections.shuffle(arcs);
        for (int i = 0; i < m; i++){
            Arc toInsert = arcs.get(i);
            matriceAdjacence[toInsert.getFrom()][toInsert.getTo()] = 1;
            if(s){
                matriceAdjacence[toInsert.getTo()][toInsert.getFrom()] = 1;
            }
        }
        return matriceAdjacence;
    }

    public static int[][] generateValuatedGraphData(int n, int m, boolean s, int min, int max) {
        Random rand = new Random(0);
        int[][] ret = new int[n][n];

        for (int i = 0; i < ret.length; i++) {
            for (int j = 0; j < ret.length; j++) {
                ret[i][j] = Integer.MAX_VALUE;
            }
        }

        ArrayList<Arc> arcs = new ArrayList<Arc>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(j != i) {
                    arcs.add(new Arc(i, j));
                }
            }
        }
        Collections.shuffle(arcs);
        for (int i = 0; i < m; i++) {
            Arc toInsert = arcs.get(i);
            int poids = rand.nextInt(max+1-min) + min;
            ret[toInsert.getFrom()][toInsert.getTo()] = poids;
            if (s) {
                ret[toInsert.getTo()][toInsert.getFrom()] = poids;
            }
        }

        return ret;
    }

    public static void showMatrice(int[][] matrice){
        System.out.println(" ");
        for (int i = 0; i < matrice.length ; i++){
            showLine(matrice[i]);
        }
    }

    public static void showLine(int[] ligne){
        System.out.println(" ");
        for(int i = 0; i < ligne.length; i++){
            System.out.print(ligne[i] + " ");
        }
    }

    public static boolean MatrixEqualsAnotherMatrix(int[][] array1, int[][] array2){
        boolean equals = true;
        if(array1.length != array2.length){
            equals = false;
        }else{
            for(int i = 0; i < array1.length ; i++){
                if(!Arrays.equals(array1[i], array2[i])){
                    equals = false;
                    break;
                }
            }
        }
        return equals;
    }

    public static int[] ListIntegerToArrayInteger(List<Integer> list){
        int[] ret = new int[list.size()];
        for(int i = 0; i < list.size(); i++){
            ret[i] = list.get(i);
        }
        return ret;
    }

    public static int explorerSommet(int s, ArrayList<Integer> atteint, IUndirectedGraph graphe, int compteur, int[] debut , int[] fin) {
        int res = compteur;
        for (int t : graphe.getNeighbors(s)) {
            if (!atteint.contains(t)) {
                atteint.add(t);
                res ++ ;
                debut[t] = res;
                System.out.println("debut de "+ t + " : " + debut[t]);
                res = explorerSommet(t,atteint, graphe,res, debut,fin);
                fin[t] = res;
                System.out.println("fin de "+ t + " : " + fin[t]);
            }
        }
        res++;
        return res;
    }

    public static int explorerSommet(int s, ArrayList<Integer> atteint, IDirectedGraph graphe, int compteur, int[] debut , int[] fin) {
        int res = compteur;
        for (int t : graphe.getSuccessors(s)) {
            if (!atteint.contains(t)) {
                atteint.add(t);
                res ++ ;
                debut[t] = res;
                System.out.println("debut de " + t + " : " + debut[t]);
                res = explorerSommet(t,atteint, graphe,res,debut,fin);
                fin[t] = res;
                System.out.println("fin de "+ t +" : " + fin[t]);
            }
        }
        res++;
        return res;
    }

    public static ArrayList<Integer> explorerUDGraphProfondeur(IUndirectedGraph graphe) {
        int[] debut = new int[graphe.getNbNodes()];
        int[] fin = new int[graphe.getNbNodes()];
        int compteur = 0;
        ArrayList<Integer> atteint = new ArrayList<Integer>();
        int i = 0;
        while (i < graphe.getNbNodes() && atteint.size() < graphe.getNbNodes()) {
            if (!atteint.contains(i)) {
                atteint.add(i);
                debut[i] = compteur;
                compteur++;
                System.out.println("debut de " + i + " : " + debut[i]);
                for (int s : graphe.getNeighbors(i)) {
                    if (!atteint.contains(s)) {
                        atteint.add(s);
                        debut[s] = compteur;
                        System.out.println("début de " + s + " : " + debut[s]);
                        compteur = explorerSommet(s, atteint, graphe, compteur, debut, fin);
                        fin[s] = compteur;
                        compteur++;
                        System.out.println("fin de " + s + " : " + fin[s]);
                    }
                }
                fin[i] = compteur;
                compteur++;
                System.out.println("fin de " + i + " : " + fin[i]);
            }
            i++;
        }
        return atteint;
    }

        /**
         * Exploration en profondeur d'un graphe oriente
         * @param graphe le graphe que vous voulez explorer
         * @return le parcours du graphe
         */
        public static ArrayList<Integer> explorerDGraphEnProfondeur(IDirectedGraph graphe) {
            int[] debut = new int[graphe.getNbNodes()];
            int[] fin = new int[graphe.getNbNodes()];
            int compteur = 0 ;
            ArrayList<Integer> atteint = new ArrayList<Integer>();
            int i = 0;
            while(i < graphe.getNbNodes() || atteint.size() < graphe.getNbNodes()){
                if(!atteint.contains(i)){
                    debut[i] = compteur;
                    compteur ++;
                    System.out.println("debut de "+i + " : " + debut[i]);
                    atteint.add(i);
                    for (int s : graphe.getSuccessors(i) ) {
                        if (!atteint.contains(s)) {
                            atteint.add(s);
                            System.out.println("debut de "+s+ " : " + compteur);
                            debut[s] = compteur;
                            compteur = explorerSommet(s, atteint, graphe,compteur, debut, fin);
                            fin[s] = compteur;
                            compteur++;
                            System.out.println("fin de " + s + " : " + fin[s]);
                        }
                    }

                    fin[i] = compteur;
                    compteur++;
                    System.out.println("fin de " + i + " : "+ fin[i]);
                }
                i++;
            }



            return atteint;
        }

        /**
         * Exploration en largeur d'un graphe non oriente
         * @param graphe le graphe que vous voulez explorer
         * @param n le noeud à partir duquel vous voulez commencer l'exploration
         * @return le parcours du graphe
         */
        public static LinkedList<Integer> explorerUDGraphEnLargeur(IUndirectedGraph graphe, int n){
            int[] debut = new int[graphe.getNbNodes()];
            int[] fin = new int[graphe.getNbNodes()];
            boolean[] mark = new boolean[graphe.getNbNodes()];
            LinkedList<Integer> parcours = new LinkedList<Integer>(); // LinkedList -> FIFO
            LinkedList<Integer> noeuds = new LinkedList<Integer>();
            int compteur = 0;

            for(int i = 0 ; i<mark.length ; i++){
                mark[i] = false; //on crée une matrice de marqueurs
            }
            mark[n] = true;      //on marque que n a été visité
            parcours.add(n);	 //on ajoute n au parcours

            noeuds.add(n);		 //on ajoute n aux sommets parcourus

            while(!noeuds.isEmpty()){
                int x = noeuds.getFirst();
                debut[x] = compteur;
                compteur++;
                System.out.println("le début de " + x + " : " + debut[x]);
                noeuds.removeFirst();

                for (int y : graphe.getNeighbors(x)){
                    if(!mark[y]){

                        mark[y] = true;
                        parcours.add(y);
                        noeuds.add(y);
                    }
                }
                fin[x] = compteur;
                compteur++;
                System.out.println("la fin de " + x + " : " + fin[x]);
            }

            int i = 0;
            while(i<graphe.getNbNodes() && !isVectorFullTrue(mark)){
                if(!mark[i]){
                    System.out.println("le graphe n'était pas connexe, on a donc un autre arbre, il débute après : "+ parcours.getLast());

                    mark[i] = true;
                    parcours.add(i);
                    noeuds.add(i);
                    while(!noeuds.isEmpty()){
                        int x = noeuds.getFirst();
                        compteur++;
                        debut[x] = compteur;
                        System.out.println("le début de " + x + " : " + debut[x]);
                        noeuds.removeFirst();

                        for (int y : graphe.getNeighbors(x)){
                            if(!mark[y]){
                                mark[y] = true;
                                parcours.add(y);
                                noeuds.add(y);
                            }
                        }
                        compteur++;
                        fin[x] = compteur;
                        System.out.println("la fin de " + x + " : " + fin[x]);
                    }
                    i++;
                }
            }
            return parcours;
        }
        /**
         * Exploration en largeur d'un graphe oriente
         * @param graphe le graphe que vous voulez explorer
         * @param n le noeud à partir duquel vous voulez commencer l'exploration
         * @return le parcours du graphe
         */
        public static LinkedList<Integer> explorerDGraphLargeur(IDirectedGraph graphe, int n){
            int[] debut = new int[graphe.getNbNodes()];
            int[] fin = new int[graphe.getNbNodes()];
            boolean[] mark = new boolean[graphe.getNbNodes()];
            LinkedList<Integer> parcours = new LinkedList<Integer>();
            LinkedList<Integer> noeuds = new LinkedList<Integer>();//LinkedList -> FIFO
            int compteur = 0;

            for(int i = 0 ; i<mark.length ; i++){
                mark[i] = false; //on crée une matrice de marqueurs
            }
            mark[n] = true;      //n a bien été visité
            parcours.add(n);	 //on ajoute n au parcours

            noeuds.add(n);		 //on ajoute n au noeuds à visiter

            debut[n] = compteur; //on commence a analyser n
            System.out.println("le début de " + n + " : " + debut[n]);
            while(!noeuds.isEmpty()){
                int x = noeuds.getFirst(); //on prend le premier de la liste de noeuds atteints
                noeuds.removeFirst();	   //on l'enlève des noeuds à visiter

                for (int y : graphe.getSuccessors(x)){ //on prend les successeurs du noeud visité
                    if(!mark[y]){
                        compteur++;
                        debut[y] = compteur; //on commence a analyser n
                        System.out.println("le début de " + y + " : " + debut[y]);
                        mark[y] = true;
                        parcours.add(y);
                        noeuds.add(y);


                    }
                }
                compteur++;
                fin[x] = compteur;
                System.out.println("la fin de " + x + " : " + fin[x]);
            }


            int i = 0;
            while(i<graphe.getNbNodes() && !isVectorFullTrue(mark)){ //tant qu'on n'a pas répertorié tous les sommets (graphe non - connexe)
                if(!mark[i]){
                    //on annonce quand le nouvel arbre démarre
                    System.out.println("le graphe n'était pas connexe, on a donc un autre arbre, il débute après : "+ parcours.getLast());

                    mark[i] = true;
                    parcours.add(i);
                    noeuds.add(i);
                    while(!noeuds.isEmpty()){
                        int x = noeuds.getFirst();
                        compteur++;
                        debut[x] = compteur;
                        System.out.println("le début de " + x + " : " + debut[x]);
                        noeuds.removeFirst();

                        for (int y : graphe.getSuccessors(x)){
                            if(!mark[y]){
                                mark[y] = true;
                                parcours.add(y);
                                noeuds.add(y);
                            }
                        }
                        compteur++;
                        fin[x] = compteur;
                        System.out.println("la fin de " + x + " : " + fin[x]);
                    }
                }
                i++;

            }
            //on retourne une forêt si le graphe n'est pas connexe, un arbre sinon
            return parcours;
        }

    // renvoie true si toutes les occurences de vecteur sont trues, false sinon
    public static boolean isVectorFullTrue(boolean[] vecteur){
        boolean res = true;
        int i = 0;
        while(i<vecteur.length && res){
            res= vecteur[i] ;
            i++;
        }
        return res;
    }

    public static String arrayToString(int[] tab) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        for (int i = 0 ; i < tab.length ; i++) {
            a.add(tab[i]);
        }
        return a.toString();
    }

}
