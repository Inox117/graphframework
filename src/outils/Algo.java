package outils;

import arbre.TasBinaireAretes;
import graphe.*;

import java.util.*;

public class Algo {

//    public static void prim(IUndirectedGraph graphe){
//        // Version 1 : g est forcément connexe
//        // version 2 : g pas forcément connexe ;
//        // 		on retourne lors l'arbre minimal
//        //		couvrant la composante connexe contenant le sommet initial (ici 0)
//        boolean encoreUnVoisinNonAtteint = true; // version 2 : g pas nécessairement connexe
//        int n = graphe.getNbNodes();
//        ValuatedUndirectedMatrix st = new ValuatedUndirectedMatrix(n); // st pour spanning tree
//        ArrayList<Integer> sommetsAtteints = new ArrayList<Integer>();
//        int xMin=0;
//        int yMin=0;
//        sommetsAtteints.add(0); // premier sommet
//        //	while (sommetsAtteints.size() < n) { // Version 1
//        while (encoreUnVoisinNonAtteint) { // version 2
//            encoreUnVoisinNonAtteint = false; // version 2
//            int valeur = Integer.MAX_VALUE;
//            for (int x : sommetsAtteints) {
//                for (int y : graphe.getNeighbors(x)) {
//                    //		    if (!sommetsAtteints.contains(y) && g.getValue(x, y) < valeur) { // version 1
//                    if (!sommetsAtteints.contains(y)) { // version 2
//                        encoreUnVoisinNonAtteint = true; // il reste au moins un voisin non encore atteint (y) ; version 2
//                        if (graphe.getValue(x, y) < valeur) { // version 2
//                            valeur = graphe.getValue(x, y);
//                            xMin = x;
//                            yMin = y;
//                        }
//                    }
//                }
//            }
//            if (encoreUnVoisinNonAtteint) { // version 2
//                sommetsAtteints.add(yMin);
//                st.addEdge(xMin, yMin, g.getValue(xMin, yMin));
//            }
//
//        }
//        return st;
//    }

    public static ValuatedUndirectedMatrix prim(IUndirectedGraph graphe){
        //INITIALISATION
        int indexDepart = 0;
        int n = graphe.getNbNodes();
        boolean visited[] = new boolean[n];		//marqueurs, permettant de déterminer si le sommet à été visité
        int[][] cout = graphe.toAdjacencyMatrix();
        Set<Integer> sommets = new HashSet<Integer>(); // Set de tous les sommets non visités
        ValuatedUndirectedMatrix res = new ValuatedUndirectedMatrix(n);
        TasBinaireAretes tasBinaireAretes = new TasBinaireAretes(cout[indexDepart][0],indexDepart,0); //tas binaire classant les arêtes par couts croissants

        for (int i = 0 ; i<n ; i++){
            sommets.add(i);
            visited[i] = false;
            if(i!=indexDepart){
                res.addEdge(i, i, 0);
            }
        }
        for (int i : graphe.getNeighbors(indexDepart)){
            tasBinaireAretes.ajouterFeuille(cout[indexDepart][i], indexDepart, i);
        }
        visited[indexDepart] = true;
        sommets.remove(indexDepart);

        //ON APPLIQUE L'ALGORITHME
        while(!sommets.isEmpty()){
            int[] tab = tasBinaireAretes.removeFirst();	//on prend la premiere arête du tas, "la moins chere"
            if(!visited[tab[3]]){
                visited[tab[3]] = true;
                sommets.remove(tab[3]);
                res.addEdge(tab[2], tab[3], tab[1]);
                for(int s : graphe.getNeighbors(tab[3])){	//on ajoute les nouvelles arêtes possibles au tas, sans faire de cycle
                    if(!visited[s]){
                        tasBinaireAretes.ajouterFeuille(cout[tab[3]][s], tab[3], s);
                    }
                }
            }
        }
        System.out.println("\n\tLa racine de l'arbre recouvrant \n\tde poids minimal est : " + indexDepart);
        System.out.println("l'arbre en question est défini par la matrice suivante : ");
        GraphTools.showMatrice(res.toAdjacencyMatrix());
        return res;
    }

    public static ValuatedDirectedMatrix prim(IDirectedGraph graphe){
        int indexDepart = 0;
        int n = graphe.getNbNodes();
        boolean visited[] = new boolean[n];
        int[][] cout = graphe.toAdjacencyMatrix();
        Set<Integer> sommets = new HashSet<Integer>();
        ValuatedDirectedMatrix res = new ValuatedDirectedMatrix(n);
        TasBinaireAretes tasBinaireArete = new TasBinaireAretes(cout[indexDepart][0],indexDepart,0);
        for (int i = 0 ; i<n ; i++){
            sommets.add(i);
            visited[i] = false;
            if(i!=indexDepart){
                res.addArc(i, i, Integer.MAX_VALUE);
            }
        }
        for (int i : graphe.getSuccessors(indexDepart)){
            tasBinaireArete.ajouterFeuille(cout[indexDepart][i], indexDepart, i);
        }


        visited[indexDepart] = true;
        sommets.remove(indexDepart);

        while(!sommets.isEmpty()){
            int[] tab = tasBinaireArete.removeFirst();
            if(!visited[tab[3]]){
                visited[tab[3]] = true;
                sommets.remove(tab[3]);
                res.addArc(tab[2], tab[3], tab[1]);
                for(int s : graphe.getSuccessors(tab[3])){
                    if(!visited[s]){
                        tasBinaireArete.ajouterFeuille(cout[tab[3]][s], tab[3], s);
                    }
                }
            }
        }
        System.out.println("\n\tLa racine de l'arbre recouvrant \n\tde poids minimal est : " + indexDepart);
        System.out.println("l'arbre en question est défini par la matrice suivante : ");
        GraphTools.showMatrice(res.toAdjacencyMatrix());
        return res;
    }

    public static int[][] floyd(IUndirectedGraph graphe){
        int[][] cout = graphe.toAdjacencyMatrix();
        int n = graphe.getNbNodes();
        //Initialisation
        int[][] p = new int[n][n];	//Matrice des prédécesseurs
        int[][] v = new int[n][n];	//Matrice des plus courts chemins
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                if (i==j) {
                    v[i][j]=0;
                    p[i][j]=i;
                } else {
                    v[i][j]=Integer.MAX_VALUE;
                    p[i][j]=0;
                }
            }

            for (int t : graphe.getNeighbors(i)) {
                v[i][t]=cout[i][t];
                p[i][t]=i;
            }
        }
        // calcul des matrices successives
        for (int k=0; k<n; k++) {
            for (int i=0; i<n; i++) {
                for (int j=0; j<n; j++) {
                    if(v[i][k] != Integer.MAX_VALUE && v[k][j] != Integer.MAX_VALUE){
                        if (v[i][k]+v[k][j]<v[i][j]) {
                            v[i][j]= v[i][k]+v[k][j];
                            p[i][j]=p[k][j];
                        }
                    }
                }
            }
        }
        return v;
    }

    public static int[][] floyd(IDirectedGraph graphe){
        int[][] cout = graphe.toAdjacencyMatrix();
        int n = graphe.getNbNodes();
        //Initialisation
        int[][] p = new int[n][n];	//Matrice des prédécesseurs
        int[][] v = new int[n][n];	//Matrice des plus courts chemins
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                if (i==j) {
                    v[i][j]=0;
                    p[i][j]=i;
                } else {
                    v[i][j]=Integer.MAX_VALUE;
                    p[i][j]=0;
                }
            }

            for (int t : graphe.getSuccessors(i)) {
                v[i][t]=cout[i][t];
                p[i][t]=i;
            }
        }
        // calcul des matrices successives
        for (int k=0; k<n; k++) {
            for (int i=0; i<n; i++) {
                for (int j=0; j<n; j++) {
                    if(v[i][k] != Integer.MAX_VALUE && v[k][j] != Integer.MAX_VALUE){
                        if (v[i][k]+v[k][j]<v[i][j]) {
                            v[i][j]= v[i][k]+v[k][j];
                            p[i][j]=p[k][j];
                        }
                    }
                }
            }
        }
        return v;
    }

}
