package graphe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quentin on 27/01/15.
 */
public class AdjacencyListUndirectedGraph implements IUndirectedGraph {

    private List<Integer> node;
    private List<Integer> succ;

    /**
     * constructeur qui prend en argument une matrice
     * @param matriceAdjacence matrice
     */
    public AdjacencyListUndirectedGraph(int[][] matriceAdjacence) {
        int indiceSucc = 0;
        this.node = new ArrayList<Integer>();
        this.succ = new ArrayList<Integer>();

        for(int i = 0 ; i < matriceAdjacence.length; i++){
            node.add(indiceSucc);
            for (int j = 0 ; j < matriceAdjacence.length; j++){
                if(matriceAdjacence[i][j]==1){
                    succ.add(j);
                    indiceSucc++;
                }
            }
        }
        this.node.add(indiceSucc);
        this.succ.add(null);
    }

    /**
     * Constructeur qui prend en argument un graph non dirigé
     * @param undirectedGraph graphe
     */
    public AdjacencyListUndirectedGraph(IUndirectedGraph undirectedGraph) {
        this.node = new ArrayList<Integer>();
        this.succ = new ArrayList<Integer>();

        int[][] matrice = undirectedGraph.toAdjacencyMatrix();
        AdjacencyListUndirectedGraph conversion = new AdjacencyListUndirectedGraph(matrice);

        this.node = conversion.getNode();
        this.succ = conversion.getSucc();

    }

    @Override
    public int getEdges() {
        return (succ.size() - 1) / 2;
    }

    @Override
    public boolean isEdge(int x, int y) {
        boolean ret = false;
        int startSucc = this.node.get(x);
        int stopSucc = this.node.get(x+1);

        for (int i = 0; i < stopSucc - startSucc; i++){
            if(this.succ.get(startSucc + i) == y){
                ret = true;
                break;
            }
        }

        return ret;
    }

    @Override
    public void removeEdge(int x, int y) {
        if(isEdge(x,y)){
            for (int i = 0; i < node.get(x + 1) - node.get(x) ; i++){
                if (succ.get(i+node.get(x)) == y){
                    succ.remove(i + node.get(x));
                }
            }
            for (int i = 1; i < node.size() - x ; i++) {
                node.set(x + i, node.get(i + x) - 1);
            }
            removeEdge(y, x);
        }
    }

    @Override
    public void addEdge(int x, int y) {
        if(!isEdge(x,y)){
            for (int i = 1; i < node.size() - x; i++){
                node.set(x + i, node.get(x+i) +1);
            }
            Integer tmp = null;
            Integer toAdd = y;
            for (int i = 0 ; i < succ.size() - node.get(x); i++){
                tmp = succ.get(node.get(x) + i);
                succ.set(node.get(x) + i, toAdd);
                toAdd = tmp;
            }
            succ.add(null);
            addEdge(y, x);
        }
    }

    @Override
    public int[] getNeighbors(int x) {
        List<Integer> neighbors = new ArrayList<Integer>();
        for (int i = 0; i < node.get(x+1) - node.get(x) ; i++){
            neighbors.add(succ.get(node.get(x) + i));
        }
        int[] ret = new int[neighbors.size()];
        for(int i = 0; i < neighbors.size() ; i++){
            ret[i] = neighbors.get(i);
        }
        return ret;
    }

    @Override
    public int getNbNodes() {
        return node.size() - 1;
    }

    @Override
    public int[][] toAdjacencyMatrix() {
        int[][] matriceAdjacence = new int[getNbNodes()][getNbNodes()];
        for(int i = 0 ; i < getNbNodes(); i++){
            int[] voisins = getNeighbors(i);
            for(int j = 0 ; j < voisins.length; j++){
//                if(isEdge(i,j)){
                    matriceAdjacence[i][voisins[j]] = 1;
//                }
            }
        }
        return matriceAdjacence;
    }

    public List<Integer> getNode() {
        return node;
    }

    public void setNode(List<Integer> node) {
        this.node = node;
    }

    public List<Integer> getSucc() {
        return succ;
    }

    public void setSucc(List<Integer> succ) {
        this.succ = succ;
    }
}
