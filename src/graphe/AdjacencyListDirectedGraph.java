package graphe;

import outils.GraphTools;

import java.util.ArrayList;
import java.util.List;

public class AdjacencyListDirectedGraph implements IDirectedGraph {

    private List<Integer> node;
    private List<Integer> succ;

    /**
     * constructeur qui prend en argument une matrice
     * @param matriceAdjacence matrice
     */
    public AdjacencyListDirectedGraph(int[][] matriceAdjacence) {
        int indiceSucc = 0;
        this.node = new ArrayList<Integer>();
        this.succ = new ArrayList<Integer>();

        for(int i = 0 ; i < matriceAdjacence.length; i++){
            node.add(indiceSucc);
            for (int j = 0 ; j < matriceAdjacence.length; j++){
                if(matriceAdjacence[i][j]==1){
                    succ.add(j);
                    indiceSucc++;
                }
            }
        }
        this.node.add(indiceSucc);
        this.succ.add(null);
    }

    /**
     * Constructeur qui prend en argument un graph non dirigé
     * @param undirectedGraph graphe
     */
    public AdjacencyListDirectedGraph(IUndirectedGraph undirectedGraph) {
        this.node = new ArrayList<Integer>();
        this.succ = new ArrayList<Integer>();

        int[][] matrice = undirectedGraph.toAdjacencyMatrix();
        AdjacencyListDirectedGraph conversion = new AdjacencyListDirectedGraph(matrice);

        this.node = conversion.getNode();
        this.succ = conversion.getSucc();

    }

    /**
     * constructeur prenant en argument une liste de noeud et de succésseur
     * @param node liste de noeud
     * @param succ liste de succésseur
     */
    public AdjacencyListDirectedGraph(List<Integer> node, List<Integer> succ){
        this.node = node;
        this.succ = succ;
    }



    public int getNbArcs() {
        return succ.size() - 1;
    }

    @Override
    public boolean isArc(int from, int to) {
        boolean ret = false;
        int startSucc = this.node.get(from);
        int stopSucc = this.node.get(from+1);

        for (int i = 0; i < stopSucc - startSucc; i++){
            if(this.succ.get(startSucc + i) == to){
                ret = true;
                break;
            }
        }

        return ret;
    }

    @Override
    public void removeArc(int from, int to) {
        if(isArc(from, to)){
            for (int i = 0; i < node.get(from + 1) - node.get(from) ; i++){
                if (succ.get(i+node.get(from)) == to){
                    succ.remove(i + node.get(from));
                }
            }
            for (int i = 1; i < node.size() - from ; i++) {
                node.set(from + i, node.get(i + from) - 1);
            }
        }
    }

    @Override
    public void addArc(int from, int to) {
        if(!isArc(from, to)){
            for (int i = 1; i < node.size() - from; i++){
                node.set(from + i, node.get(from+i) +1);
            }
            Integer tmp = null;
            Integer toAdd = to;
            for (int i = 0 ; i < succ.size() - node.get(from); i++){
                tmp = succ.get(node.get(from) + i);
                succ.set(node.get(from) + i, toAdd);
                toAdd = tmp;
            }
            succ.add(null);
        }
    }

    @Override
    public int[] getSuccessors(int x) {
        List<Integer> successors = new ArrayList<Integer>();
        int start = node.get(x);
        int stop = node.get(x+1);
        while (start < stop){
            successors.add(succ.get(start));
            start++;
        }
        return GraphTools.ListIntegerToArrayInteger(successors);
    }

    @Override
    public int[] getPredecessors(int x){
        ArrayList<Integer> predecessors = new ArrayList<Integer>();
        ArrayList<Integer> indicesSucc = new ArrayList<Integer>();
        for (int i = 0; i < succ.size() - 1; i++) {
            if (succ.get(i) == x) {
                indicesSucc.add(i);
            }
        }
        for (int i = 0; i < indicesSucc.size(); i++) {
            for (int j = 0; j < node.size() - 1; j++) {
                if(indicesSucc.get(i) >= node.get(j) && indicesSucc.get(i) < node.get(j+1)) {
                    predecessors.add(j);
                    break;
                }
            }
        }
        return GraphTools.ListIntegerToArrayInteger(predecessors);
    }

    @Override
    public IDirectedGraph computeInverse() {
        int[] inverseNode   = new int[this.getNbNodes()+ 1];
        int[] inverseSucc   = new int[this.getNbArcs() + 1];
        int[] d             = new int[this.getNbNodes()];
        for (int i = 0; i < this.getNbArcs(); i++){
            d[this.succ.get(i)]++;
        }

        inverseNode[0] = 0;
        for(int k = 1; k < inverseNode.length; k++){
            inverseNode[k] = inverseNode[k-1] + d[k-1];
        }

        for(int k = 0; k < this.getNbNodes(); k++){
            for(int i = node.get(k); i < node.get(k+1); i++){
                inverseSucc[inverseNode[succ.get(i)+1]-d[succ.get(i)]] = k;
                d[succ.get(i)]--;
            }
        }

        ArrayList<Integer> listNode = new ArrayList<Integer>();
        for(int i = 0; i < inverseNode.length; i++){
            listNode.add(inverseNode[i]);
        }

        ArrayList<Integer> listSucc = new ArrayList<Integer>();
        for(int i = 0; i < inverseSucc.length; i++){
            listSucc.add(inverseSucc[i]);
        }
        listSucc.set(listSucc.size()-1, null);
        return new AdjacencyListDirectedGraph(listNode, listSucc);
    }

    @Override
    public int getNbNodes() {
        return node.size() - 1;
    }

    @Override
    public int[][] toAdjacencyMatrix() {
        int[][] matriceAdjacence = new int[getNbNodes()][getNbNodes()];
        for(int i = 0 ; i < getNbNodes(); i++){
            int[] successors = getSuccessors(i);
            for(int j = 0 ; j < successors.length; j++){
                    matriceAdjacence[i][successors[j]] = 1;
            }
        }
        return matriceAdjacence;
    }

    public List<Integer> getNode() {
        return node;
    }

    public void setNode(List<Integer> node) {
        this.node = node;
    }

    public List<Integer> getSucc() {
        return succ;
    }

    public void setSucc(List<Integer> succ) {
        this.succ = succ;
    }
}
