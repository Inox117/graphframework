package graphe;

/**
 * Created by quentin on 27/01/15.
 */
public interface IGraph {
    // Returns the number of nodes in the graph (referred to as the order of the length
    public int getNbNodes();
    // Returns the adjacency matrix representation of the graph
    public int[][] toAdjacencyMatrix();
}
