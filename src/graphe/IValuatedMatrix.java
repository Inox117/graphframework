package graphe;

/**
 * Created by quentin on 10/02/15.
 */
public interface IValuatedMatrix {

    public int getValueAt(int x, int y);
    public void setValueAt(int x, int y, int value);

}
