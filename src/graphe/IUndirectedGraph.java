package graphe;

/**
 * Created by quentin on 27/01/15.
 */
public interface IUndirectedGraph extends IGraph {
    // Returns the number of edges in the graph
    public int getEdges();
    // Returns true if there is an edge between x and y
    public boolean isEdge(int x, int y);
    // Remove edge(x,y) if exists
    public void removeEdge(int x, int y);
    // Adds edge(x,y) if not already present, requires x != y
    public void addEdge(int x, int y);
    // Returns a new int array representing neighbors of node x
    public int[] getNeighbors(int x);
}
