package graphe;

public class ValuatedUndirectedMatrix implements IUndirectedGraph, IValuatedMatrix {

    private int[][] adjacencyMatrix;

    public ValuatedUndirectedMatrix(int[][] adjacencyMatrix) {
        this.adjacencyMatrix = adjacencyMatrix;
    }

    public ValuatedUndirectedMatrix(int n){
        int[][] adjacencyMatrix = new int[n][n];
        for (int i = 0 ; i< n ; i++){
            for (int j = 0 ; j < n ; j++){
                if(i!=j){
                    adjacencyMatrix[i][j] = Integer.MAX_VALUE;
                }
                else{
                    adjacencyMatrix[i][i] = 0;
                }
            }
        }
        this.adjacencyMatrix = adjacencyMatrix;

    }

    @Override
    public int getValueAt(int x , int y ){
        return this.adjacencyMatrix[x][y];
    }

    @Override
    public void setValueAt(int x, int y , int value){
        this.adjacencyMatrix[x][y] = value;
    }


    public void setAdjacencyMatrix(int[][] adjacencyMatrix) {
        this.adjacencyMatrix = adjacencyMatrix;
    }

    @Override
    public int getNbNodes() {
        return this.toAdjacencyMatrix().length;
    }

    @Override
    public int[][] toAdjacencyMatrix() {
        return this.adjacencyMatrix;
    }

    @Override
    public int getEdges() {
        int res = 0 ;
        for (int i=0; i<this.adjacencyMatrix.length;i++){
            for (int j = i; j<this.adjacencyMatrix[i].length ; j++){
                if(this.adjacencyMatrix[i][j] >=1){
                    res++;
                }
            }
        }
        return res;
    }

    @Override
    public boolean isEdge(int x, int y) {
        return this.getValueAt(x, y)<Integer.MAX_VALUE;
    }

    @Override
    //On peut retiré un coté en le mettant à Integer.MAX_VALUE car isEdge ne le verra plus comme
    public void removeEdge(int x, int y) {
        this.setValueAt(x, y, Integer.MAX_VALUE);
        this.setValueAt(y, x, Integer.MAX_VALUE);
    }

    @Override
    public void addEdge(int x, int y) {
        //pas utilisé
    }

    public void addEdge(int x, int y, int value) {
        this.setValueAt(x, y, value);
        this.setValueAt(y, x, value);
    }

    @Override
    public int[] getNeighbors(int x) {
        int compteur = 0 ;
        for (int i = 0 ; i<this.adjacencyMatrix[x].length ; i++){
            if (x!=i && this.getValueAt(x, i)<Integer.MAX_VALUE){
                compteur++;
            }
        }

        int[] voisins = new int[compteur];
        int offset = 0;
        for (int j = 0 ; j< this.adjacencyMatrix[x].length ; j++){
            if( x!=j && this.getValueAt(x, j)<Integer.MAX_VALUE){
                voisins[offset] = j;
                offset++;
            }
        }
        return voisins;
    }

}
