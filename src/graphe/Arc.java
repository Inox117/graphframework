package graphe;

/**
 * Created by quentin on 01/02/15.
 */
public class Arc {
    private int from;
    private int to;

    public Arc(int to, int from) {
        this.to = to;
        this.from = from;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
