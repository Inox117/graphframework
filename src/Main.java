import graphe.AdjacencyListDirectedGraph;
import graphe.IDirectedGraph;
import graphe.ValuatedDirectedMatrix;
import graphe.ValuatedUndirectedMatrix;
import outils.Algo;
import outils.GraphTools;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
//            int[][] matrice = GraphTools.generateGraphData(5, 4, false);
//            GraphTools.showMatrice(matrice);
//            IDirectedGraph graph = new AdjacencyListDirectedGraph(matrice);
//            ArrayList<Integer> fin =  GraphTools.explorerDGraphEnProfondeur(graph);
//            graph = graph.computeInverse();
//            ArrayList<Integer> finCompute = GraphTools.explorerDGraphEnProfondeur(graph);
//
//            System.out.println(fin);
//            System.out.println(finCompute);

        int[][] data = GraphTools.generateValuatedGraphData(5, 8, true, 1, 10);
        ValuatedUndirectedMatrix g = new ValuatedUndirectedMatrix(data);

        GraphTools.showMatrice(g.toAdjacencyMatrix());

        System.out.println();
        System.out.println();

        for (int x = 0 ; x < g.getNbNodes() ; x++) {
            System.out.println(x + ":" + GraphTools.arrayToString(g.getNeighbors(x)));
        }
//        Algo.prim(g);
        GraphTools.showMatrice(Algo.floyd(g));

    }
    
}
